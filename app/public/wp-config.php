<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1Gx0t7iKyIwhAOm2LrZy7SSpXbAinlsaR5XcYrT0Msc3xZ0mjiwLY+1gKG7nOsvw2v9N5TAu743TI1687vUlyg==');
define('SECURE_AUTH_KEY',  'Z/9oYxQ0Zs8MImQF84MmBZONIyCP1ABZVkeZkj13m/s6SPatu26DMAANSa9q3ph+to464jVt57LernB7WkLycA==');
define('LOGGED_IN_KEY',    'oN3Ob5+POm8xWKvcmcCvOhSMgf1sjlvW4GFOG1KJGG2SebCm50JqCHd2WioocfpM0EXFIubSM/on9K0fxy3izQ==');
define('NONCE_KEY',        'rj0eVAVzOIkaDZ8IN5n3WzrPHmaCXa1/gYtwO1GcgL9VhIS8OQs1PRrcWMwIiUZPArpssORIxvthTjcGSmCthw==');
define('AUTH_SALT',        'DAt+BWTKR1LnsldGv5IDfVhaG9EQcuO5xkYDJRT1rGuJ+TBOuDQgS0MH03/f+jRmrtltFK74/olP0CAtXdZ2Ug==');
define('SECURE_AUTH_SALT', '+C3tNIQirEGH4YIemFOw+I4Gy3kPG8FS3p9pBE8XsA1FlZiMwY1rp84T5qG8iphs5dcQXQp/ayawI/PrTDC1tw==');
define('LOGGED_IN_SALT',   'iiEUljZWojYdfQnBcwKjwWFHCUoU95VgmIkNwIIA9Ep892sOqwecGlkJPQLj5uXx9048OPxirfymvTtceY8Epg==');
define('NONCE_SALT',       'datdiVEPMUIJGTRr5UP8gpHte6/SojwKN3eb2tSuLKGVPxdUEFbblUa3wQK5Mj5RfQ+vWeyYHaKQxwKt1gmgXA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
